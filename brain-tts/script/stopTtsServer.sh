#!/usr/bin/bash

if [ "0" == "$#" ]; then
        export TTS_PROFILE="dev"
else
        export TTS_PROFILE="$1"
fi

export P_NM="tts_server_${TTS_PROFILE}_$(hostname)"
count=`ps -ef | grep ${P_NM}|grep -v grep |awk '{print $2}'|wc -l`

if [ $count -gt 0 ]; then
        pid=`ps -ef | grep  ${P_NM}|grep -v grep |awk '{print $2}'`
        echo "shutting down ${P_NM} process... ${pid}";
        kill $pid;
else
        echo "${P_NM} is not running ...";
        exit 0;
fi
