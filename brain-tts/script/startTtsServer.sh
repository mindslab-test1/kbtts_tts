#!/usr/bin/bash

if [ "0" == "$#" ]; then
        export TTS_PROFILE="dev"
else
        export TTS_PROFILE="$1"
fi

export MEM_ARGS="-verbosegc -Xms512m -Xmx4096m"
export P_NM="tts_server_${TTS_PROFILE}_$(hostname)"

echo "profile ${TTS_PROFILE}"

count=`ps -ef | grep ${P_NM} |grep -v grep |awk '{print $2}'|wc -l`

if [ $count -gt 0 ]; then
        echo "${P_NM} Process is already running ...";
else

${JAVA_HOME}/bin/java -jar  -DNODE_NAME=${P_NM} \
-Dlog4j.configurationFile=config/tts/log4j2.xml \
-Dspring.config.location=config/tts/application-${TTS_PROFILE}.properties \
-Dspring.profiles.active=${TTS_PROFILE} \
brain-tts-stream.jar ${MEM_ARGS} maum.brain.tts.Application > /dev/null 2>&1 &

fi