package maum.brain.tts.client;


import com.google.common.util.concurrent.ListenableFuture;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import maum.brain.tts.AcousticGrpc;
import maum.brain.tts.Tts;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static junit.framework.TestCase.fail;

public class TestClient {

//    @Test
//    public void testTts(){
//        System.out.println("========================testTts()====================");
//
//        String ip = "127.0.0.1";
//        int tacoPort = 30001, wavPort = 35001;
//
//        TTSClient client = new TTSClient(ip, tacoPort, ip, wavPort, 120);
//        try {
////            String inText = "Citing insufficient progress on the issue of denuclearization, President Donald Trump nixed plans for Secretary of State Mike Pompeo to make what would have been his fourth visit to North Korea Friday, canceling next week's trip just one day after it was announced by Pompeo.";
//            String inText = "Hello everyone. How are you??";
////            String inText = "President Donald Trump nixed plans for Secretary of State Mike Pompeo to make what would have been his fourth visit to North Korea Friday,";
////            String inText = "President Trump met with other leaders at the Group of 20 conference.";
////            String[] simpleTxt = inText.split("\\.");
////            if(simpleTxt != null && simpleTxt.length > 0){
////                for(int i=0;i<simpleTxt.length;i++){
////                    String partTxt = simpleTxt[i];
////                    if(!StringUtils.isEmpty(partTxt) && partTxt.length() > 100){
////                        String[] subs = partTxt.split(",");
////                        int idx = 0;
////                        for(String sub:subs){
////                            client.doTts(sub, "sub_"+idx+".wav");
////                        }
////                    }else {
////                        client.doTts(partTxt, "test_" + i + ".wav");
////                    }
////                }
//////                client.doTts(simpleTxt[0], "test_"+"all"+".wav");
////            }
//
//            client.getTacotronInfo();
//            client.getWavenetInfo();
//            client.doTts(inText, "test_all.wav");
//
//        }catch(Exception e){
//            fail(e.getMessage());
//        }
//    }

    @Test
    public void testTaco(){
        String tacoIp = "127.0.0.1";
        int tacoPort = 30101, wavPort = 35101;

        int speaker = 0;
        String text = "반갑습니다 고객님께서 이번에 가입하신 보험에 대해, 정확하게 설명을 듣고 가입하셨는지 확인하는, 완전 판매 모니터링을 진행하기 위해 전화드렸습니다.";

        long deadline = 300;
        ManagedChannel tacoChannel = null;
        Tts.InputText inputText = Tts.InputText.newBuilder().setSpeaker(speaker).setText(text).build();
        tacoChannel = ManagedChannelBuilder.forAddress(tacoIp, tacoPort).usePlaintext().build();

        {
            long st = System.currentTimeMillis();
            AcousticGrpc.AcousticBlockingStub tacoStub = AcousticGrpc.newBlockingStub(tacoChannel);
            List<Float> list = tacoStub.withDeadlineAfter(deadline, TimeUnit.SECONDS)
                                        .txt2Mel(inputText).getDataList();
            System.out.println("+++++++++++blocking stub " + list.size() + " " + (System.currentTimeMillis() - st));
        }

        {
            long st = System.currentTimeMillis();
            AcousticGrpc.AcousticFutureStub tacoStub = AcousticGrpc.newFutureStub(tacoChannel);
            ListenableFuture<Tts.MelSpectrogram> out = tacoStub
                                                    .withDeadlineAfter(deadline, TimeUnit.SECONDS)
                                                    .txt2Mel(inputText);
            try {
                Tts.MelSpectrogram f = out.get();
                List<Float> list = f.getDataList();
                System.out.println("+++++++++++future stub "+list.size() + " " + (System.currentTimeMillis() - st));

            }catch(Exception e){

                System.out.println("+++future error " + e.getMessage());
            }
        }
        {
            long st = System.currentTimeMillis();
            AcousticGrpc.AcousticStub tacoStub = AcousticGrpc.newStub(tacoChannel);
            final List<Float> list = new ArrayList<>();
            AtomicBoolean completed = new AtomicBoolean(false);
            AtomicBoolean error = new AtomicBoolean(false);
            tacoStub.withDeadlineAfter(deadline, TimeUnit.SECONDS)
                    .txt2Mel(inputText, new StreamObserver<Tts.MelSpectrogram>() {
                @Override
                public void onNext(Tts.MelSpectrogram melSpectrogram) {
                    list.addAll(melSpectrogram.getDataList());
                }

                @Override
                public void onError(Throwable throwable) {
//                    logger.error("callTacotron/onError/{}", throwable.getMessage());
                    error.compareAndSet(false, true);
                }

                @Override
                public void onCompleted() {
//                    logger.debug("callTacotron/onCompleted");
                    completed.compareAndSet(false, true);
                }
            });
            while (!completed.get() && !error.get()) {
                try {
                    Thread.sleep(500);
                }catch(InterruptedException e){

                }
            }
            System.out.println("+++++++async++++" + list.size() + " " + (System.currentTimeMillis() - st));
        }
    }
}
