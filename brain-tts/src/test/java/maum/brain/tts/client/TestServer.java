package maum.brain.tts.client;

import com.google.common.io.ByteStreams;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.tts.*;
import maum.common.LangOuterClass;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Iterator;

import static junit.framework.TestCase.fail;

public class TestServer {


    @Test
    public void testServerSpeak(){
        System.out.println("==================================");
        System.out.println("testServerSpeak");
        System.out.println("==================================");

        String ip = "127.0.0.1";;
        int port = 9999;

        ManagedChannel channel = ManagedChannelBuilder.forAddress(ip, port).usePlaintext().build();
        NgTtsServiceGrpc.NgTtsServiceBlockingStub stub = NgTtsServiceGrpc.newBlockingStub(channel);

        TtsRequest.Builder req = TtsRequest.newBuilder();
        req.setLang(LangOuterClass.Lang.en_US);
        req.setSampleRate(16000);
        //req.setText("Hello. it's text to speach test with tacotron and wavenet.");
        req.setSpeaker(0);
        req.setText("North Korea has accused Japan of sabotaging peace efforts on the Korean peninsula \"at any cost.\"");

        try (
                FileOutputStream fos = new FileOutputStream("aaa.wav");
                FileChannel fc = fos.getChannel();
        ){
            Iterator<TtsMediaResponse> iter = stub.speakWav(req.build());
            while(iter.hasNext()) {
                TtsMediaResponse resp = iter.next();
                ByteString data = resp.getMediaData();
                ByteBuffer buff = data.asReadOnlyByteBuffer();
                fc.write(buff);
            }

//            TtsMediaResponse resp = stub.speakWav(req.build());
//            ByteString data = resp.getMediaData();
//            ByteBuffer buff = data.asReadOnlyByteBuffer();
//            fc.write(buff);
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }
}
