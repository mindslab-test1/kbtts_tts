package maum.brain.tts.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.g2p.G2PGrpc;
import maum.brain.g2p.G2POuterClass;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class G2PClient {

    @Value("${tts.g2p.address}")
    private String g2pAddress;

    public G2PClient(){ }

    public G2PClient(String g2pAddress){ this.g2pAddress = g2pAddress; }

    public List<String> callG2p (List<String> sentenceList) {
        if (g2pAddress.isEmpty()) {
            return sentenceList;
        }

        ManagedChannel g2pChannel = ManagedChannelBuilder.forTarget(g2pAddress).usePlaintext().build();
        try {
            G2POuterClass.Grapheme inputText = G2POuterClass.Grapheme.newBuilder().addAllSentences(sentenceList).build();
            G2PGrpc.G2PBlockingStub g2pStub = G2PGrpc.newBlockingStub(g2pChannel);
            return g2pStub.transliterate(inputText).getSentencesList();
        }catch(Exception e){
            e.printStackTrace();
            return sentenceList;
        }finally{
            g2pChannel.shutdown();
        }
    }
}