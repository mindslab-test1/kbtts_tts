package maum.brain.tts.client;

import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.tts.NgTtsServiceGrpc;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.TtsRequest;
import maum.common.LangOuterClass;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Iterator;

import static junit.framework.TestCase.fail;

public class TestClient {

    public static void main(String[] args){
        System.out.println("TestClinet started.");

        System.out.println("speakWav test...");
        String ip = "127.0.0.1";
        int port = 9998;

        ManagedChannel channel = ManagedChannelBuilder.forAddress(ip, port).usePlaintext().build();
        NgTtsServiceGrpc.NgTtsServiceBlockingStub stub = NgTtsServiceGrpc.newBlockingStub(channel);

        TtsRequest.Builder req = TtsRequest.newBuilder();
        req.setLang(LangOuterClass.Lang.ko_KR);
        req.setSampleRate(16000);
        req.setSpeaker(0);
        req.setText("Hello.");
        //req.setText("North Korea has accused Japan of sabotaging peace efforts on the Korean peninsula \"at any cost.\"");
        Iterator<TtsMediaResponse> iter = stub.speakWav(req.build());
        try (
                FileOutputStream fos = new FileOutputStream("aaa.wav");
                FileChannel fc = fos.getChannel();
        ) {
            while(iter.hasNext()) {
                TtsMediaResponse resp = iter.next();
                ByteString data = resp.getMediaData();
                ByteBuffer buff = data.asReadOnlyByteBuffer();

                    fc.write(buff);

            }

//            TtsMediaResponse resp = stub.speakWav(req.build());
//            ByteString data = resp.getMediaData();
//            ByteBuffer buff = data.asReadOnlyByteBuffer();
//            fc.write(buff);
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }
}
