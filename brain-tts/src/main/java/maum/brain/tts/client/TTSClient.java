package maum.brain.tts.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import maum.brain.tts.*;
import maum.brain.tts.exception.TtsException;
import maum.brain.tts.utils.NewWaveWriter;
import maum.brain.tts.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class TTSClient {

    private Logger logger = LoggerFactory.getLogger(TTSClient.class);

    @Value("${tts.samplerate}")
    private int sampleRate;

    @Value("${tts.acoustic.address}")
    private String acousticAddress;
    @Value("${tts.acoustic.model}")
    private String acousticModel;
    @Value("#{${tts.acoustic.config}}")
    private Map<String,String> acousticConfig;

    @Value("${tts.vocoder.address}")
    private String vocoderAddress;
    @Value("${tts.vocoder.model}")
    private String vocoderModel;
    @Value("#{${tts.vocoder.config}}")
    private Map<String,String> vocoderConfig;

    @Value("${tts.job.timeout}")
    private int timeout;

    private Tts.Model acoustic;
    private Tts.Model vocoder;

    public TTSClient(){
    }

    @PostConstruct
    public void init(){
        this.acoustic = Tts.Model.newBuilder().setPath(acousticModel).putAllConfig(acousticConfig).build();
        this.vocoder = Tts.Model.newBuilder().setPath(vocoderModel).putAllConfig(vocoderConfig).build();
    }

    public Tts.MelSpectrogram callTacotron(int speaker, String text) throws TtsException{
        logger.info("callTacotron/{},{}", speaker, text);
        ManagedChannel tacoChannel = ManagedChannelBuilder.forTarget(acousticAddress).usePlaintext().build();
        try {
            Tts.InputText inputText = Tts.InputText.newBuilder().setSpeaker(speaker).setText(text).setModel(this.acoustic).build();
            AcousticGrpc.AcousticBlockingStub tacoStub = AcousticGrpc.newBlockingStub(tacoChannel);
            return tacoStub.withDeadlineAfter(timeout, TimeUnit.SECONDS).txt2Mel(inputText);
        }catch(Exception e){
            logger.error("callTacotron/{}",e.getMessage());
            e.printStackTrace();
            throw new TtsException(e);
        }finally{
            tacoChannel.shutdown();
        }
    }

    public void callWaveNetStream(List<Sentence> sentences, List<List<Float>> dataList, int dataIndex, int dataLength, StreamObserver<Tts.WavData> observer) throws TtsException{
        int random = Utils.makeRandomId();
        logger.info("callWaveNetStream/started/{}",random);

        List<Float> data = dataList.get(dataIndex);
        final int fDataIndex = dataIndex;
        try{
            final ManagedChannel waveChannel = ManagedChannelBuilder.forTarget(vocoderAddress).usePlaintext().build();
            VocoderGrpc.VocoderStub wavStub = VocoderGrpc.newStub(waveChannel);

            Tts.MelSpectrogram.Builder reqBuilder = Tts.MelSpectrogram.newBuilder();
            reqBuilder.addAllData(data);
            reqBuilder.setModel(this.vocoder);
            Tts.MelSpectrogram request = reqBuilder.build();

            wavStub.withDeadlineAfter(timeout,TimeUnit.SECONDS)
                    .mel2Wav(request, new StreamObserver<Tts.WavData>() {
                        @Override
                        public void onNext(Tts.WavData wavData) {
                            observer.onNext(wavData);
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            logger.error("callWaveNetStream/onError/{}",throwable.getMessage());
                            observer.onError(throwable);
                        }

                        @Override
                        public void onCompleted() {
                            logger.debug("callWaveNetStream/onCompleted");
//                            finishedLatch.countDown();
                            logger.info("callWaveNetStream/finished/{}",random);
                            waveChannel.shutdown();
                            try {
                                if (fDataIndex + 1 < dataLength) {
                                    for(int j=0; j<sentences.get(dataIndex).getSilenceSample();j++) {
                                        observer.onNext(Tts.WavData.newBuilder().addData(2).build());
                                    }
                                    callWaveNetStream(sentences , dataList, dataIndex + 1, dataLength, observer);
                                } else {
                                    observer.onCompleted();
                                }
                            } catch (TtsException e){
                                logger.error(e.toString());
                            }
                        }
                    });

//            finishedLatch.await();

        }catch(Exception e){
            logger.error("callWaveNet/{}",e.getMessage());
            throw new TtsException(e);
        }
    }

    public void doTtsWav(int speaker, List<Sentence> sentences, OutputStream outputStream) throws Exception {
        logger.info("doTtsWav/{},{}", speaker, sentences);

        int sampleLength = 0;

        ArrayList<List<Float>> melSpectList = new ArrayList<>();
        for (Sentence sentence : sentences) {
            Tts.MelSpectrogram mel = callTacotron(speaker, sentence.getText());
            List<Float> fAry = mel.getDataList();
            melSpectList.add(fAry);
            sampleLength += mel.getSampleLength();
        }
        if (melSpectList.size() >= 2) {
            for(int i=0;i < melSpectList.size() - 1;i++) {
                sampleLength += sentences.get(i).getSilenceSample();
            }
        }
        NewWaveWriter waveWriter = new NewWaveWriter(this.sampleRate, sampleLength * 2);
        callWaveNetStream(sentences, melSpectList, 0, melSpectList.size(), new StreamObserver<Tts.WavData>() {
            boolean bInit = true;

            @Override
            public void onNext(Tts.WavData wavData) {
                try {

                    if (bInit) {
                        byte[] buff = waveWriter.getByteBuffer();
                        outputStream.write(buff);

                        bInit = false;
                    }
                    byte[] buff = int2bytes(wavData);
                    outputStream.write(buff);
//                    outputStream.write(0);

                } catch (IOException e) {
                    onError(e);

                }
            }

            @Override
            public void onError(Throwable throwable) {
            }

            @Override
            public void onCompleted() {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    logger.info(e.getMessage());
                }
            }
        });
    }

    public void doTtsPcm(int speaker, List<Sentence> sentences, OutputStream outputStream) throws Exception {
        logger.info("doTtsPcm/{},{}", speaker, sentences);

        callStream(speaker, sentences, 0, sentences.size(), new StreamObserver<Tts.WavData>() {
            @Override
            public void onNext(Tts.WavData wavData) {
                try {
                    byte[] buff = int2bytes(wavData);
                    outputStream.write(buff);
                } catch (IOException e) {
                    onError(e);
                }
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error("doTtsPcm/onError/{}",throwable.getMessage());
            }

            @Override
            public void onCompleted() {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    onError(e);
                }
            }
        });
    }

    private void callStream(int speaker, List<Sentence> sentences, int dataIndex, int dataLength, StreamObserver<Tts.WavData> observer) throws TtsException {
        int random = Utils.makeRandomId();
        logger.info("callStream/started/{}",random);

        try {
            ManagedChannel tacoChannel = ManagedChannelBuilder.forTarget(acousticAddress).usePlaintext().build();
            ManagedChannel waveChannel = ManagedChannelBuilder.forTarget(vocoderAddress).usePlaintext().build();

            VocoderGrpc.VocoderStub wavStub = VocoderGrpc.newStub(waveChannel);

            StreamObserver<Tts.MelSpectrogram> melStreamObserver =
                    wavStub.withDeadlineAfter(timeout, TimeUnit.SECONDS)
                            .streamMel2Wav(new StreamObserver<Tts.WavData>() {
                @Override
                public void onNext(Tts.WavData wavData) {
                    observer.onNext(wavData);
                }

                @Override
                public void onError(Throwable throwable) {
                    logger.error("callStream/onError/{}",throwable.getMessage());
                    observer.onError(throwable);
                }

                @Override
                public void onCompleted() {
                    logger.debug("callStream/onCompleted");
                    logger.info("callStream/finished/{}", random);
                    waveChannel.shutdown();
                    try {
                        if (dataIndex + 1 < dataLength) {
                            for(int j=0; j<sentences.get(dataIndex).getSilenceSample();j++) {
                                observer.onNext(Tts.WavData.newBuilder().addData(2).build());
                            }
                            callStream(speaker, sentences, dataIndex + 1, dataLength, observer);
                        } else {
                            observer.onCompleted();
                        }
                    } catch (TtsException e){
                        logger.error(e.toString());
                    }
                }
            });

            String text = sentences.get(dataIndex).getText();
            Tts.InputText inputText =
                    Tts.InputText.newBuilder()
                            .setSpeaker(speaker)
                            .setText(text)
                            .setModel(this.acoustic).build();

            AcousticGrpc.AcousticStub tacoStub = AcousticGrpc.newStub(tacoChannel);

            tacoStub.withDeadlineAfter(timeout, TimeUnit.SECONDS).streamTxt2Mel(inputText,  new StreamObserver<Tts.MelSpectrogram>() {
                boolean isStart = true;

                @Override
                public void onNext(Tts.MelSpectrogram melSpectrogram) {
                    if (isStart) {
                        melSpectrogram = Tts.MelSpectrogram.newBuilder()
                                .setModel(vocoder)
                                .addAllData(melSpectrogram.getDataList())
                                .build();
                        isStart = false;
                    }
                    melStreamObserver.onNext(melSpectrogram);
                }

                @Override
                public void onError(Throwable throwable) {
                    melStreamObserver.onError(throwable);
                }

                @Override
                public void onCompleted() {
                    melStreamObserver.onCompleted();
                    tacoChannel.shutdown();
                }
            });
        }catch(Exception e){
            logger.error("callStream/{}",e.getMessage());
            throw new TtsException(e);
        }
    }

    private byte[] int2bytes(Tts.WavData wavData) {
        byte[] buff = new byte[wavData.getDataCount() * 2];
        List<Integer> list = wavData.getDataList();
        for (int pos = 0; pos < wavData.getDataCount(); pos++) {
            NewWaveWriter.setValue(buff, pos * 2, (short) ((int) list.get(pos)));
        }
        return buff;
    }
}
