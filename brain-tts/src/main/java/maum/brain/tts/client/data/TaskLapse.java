package maum.brain.tts.client.data;

import java.io.Serializable;

public class TaskLapse implements Serializable {
    protected String lapName;
    protected long timestamp;

    public TaskLapse(){

    }
    public TaskLapse(String name, long timestamp){
        this.lapName = name;
        this.timestamp = timestamp;
    }

    public String getLapName() {
        return lapName;
    }

    public void setLapName(String lapName) {
        this.lapName = lapName;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "TaskLapse{" +
                "lapName='" + lapName + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
