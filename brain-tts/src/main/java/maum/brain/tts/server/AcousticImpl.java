package maum.brain.tts.server;

import io.grpc.stub.StreamObserver;
import maum.brain.tts.AcousticGrpc;
import maum.brain.tts.Tts;
import org.springframework.stereotype.Component;

@Component
public class AcousticImpl extends AcousticGrpc.AcousticImplBase {
    @Override
    public void txt2Mel(Tts.InputText request, StreamObserver<Tts.MelSpectrogram> responseObserver) {
        super.txt2Mel(request, responseObserver);
    }
}
