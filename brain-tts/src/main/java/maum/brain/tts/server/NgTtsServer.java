package maum.brain.tts.server;

import com.google.protobuf.ByteString;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import maum.brain.tts.*;
import maum.brain.tts.client.TTSClient;
import maum.brain.tts.exception.TtsException;
import maum.brain.tts.preprocess.IPreProcess;
import maum.brain.tts.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class NgTtsServer extends NgTtsServiceGrpc.NgTtsServiceImplBase {

    private Logger logger = LoggerFactory.getLogger(NgTtsServer.class);

    @Value("${tts.maxlength}")
    private int maxlength;

    @Value("${tts.maxspeaker}")
    private int maxSpeaker;

    @Value("${tts.samplerate}")
    private int samplerate;

    @Autowired
    private IPreProcess preProcess;

    @Autowired
    private TTSClient client;

    @Autowired
    private JobStatusManager statusManager;

    public NgTtsServer() {
    }

    @Override
    public void preprocess(PreprocessingRequest request, StreamObserver<PreprocessingResponse> responseObserver) {
        String rpcName = "preprocess";
        logger.info("{}/{}", rpcName, request.getText());
        String reqText = request.getText();
        try {
            ArrayList<String> textList = preProcess.preProcessText(reqText);
            String text = String.join("\n", textList);

            responseObserver.onNext(PreprocessingResponse.newBuilder().setText(text).build());
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error("{}: {}", rpcName, e.getMessage());
            responseObserver.onError(
                    getGrpcException(CommonCode.TTS_ERR_SVC_ERROR, e.getMessage())
            );
        }
    }

    @Override
    public void speakWav(TtsRequest request, StreamObserver<TtsMediaResponse> responseObserver) {
        String rpcName = "speakWav";
        //set job id
        String jobId = Utils.makeJobId();
        logger.info("{}/{},{}/{}", rpcName, request.getSpeaker(), request.getText(), jobId);
        putJobStatus(jobId, CommonCode.JOB_STATUS.started);

        if(StringUtils.isEmpty(request.getText())){
            responseObserver.onError(
                    getGrpcException(CommonCode.TTS_ERR_SVC_ERROR, "Empty string requested.")
            );
            logger.info("{}/finished/empty request/{}", rpcName, jobId);
            putJobStatus(jobId, CommonCode.JOB_STATUS.error);
            return;
        }

        if(request.getText().length() > maxlength){
            logger.info("{}/finished/{}/{}", rpcName, "exceeded maxlength", jobId);
            putJobStatus(jobId, CommonCode.JOB_STATUS.error);
            responseObserver.onError(
                    getGrpcException(
                            CommonCode.TTS_ERR_SVC_EXCEEDED_TEXT_LENGTH,
                            CommonCode.TTS_ERR_MSG_SVC_EXCEEDED_TEXT_LENGTH)
            );
            return;
        }

        if(request.getSpeaker() >= maxSpeaker){
            logger.info("{}/finished/{}/{}", rpcName, "exceeded maxspeaker", jobId);
            putJobStatus(jobId, CommonCode.JOB_STATUS.error);
            responseObserver.onError(
                    getGrpcException(
                            CommonCode.TTS_ERR_SVC_EXCEEDED_SPEAKER,
                            CommonCode.TTS_ERR_MSG_SVC_EXCEEDED_SPEAKER)
            );
            return;
        }

        putJobStatus(jobId, CommonCode.JOB_STATUS.running);

        try {
            ArrayList<String> textList = preProcess.preProcessText(request.getText());
            List<Sentence> sentences = new ArrayList<>();
            for (String text : textList) {
                String tmp = text.substring(text.length()-2);
                int silenceSample = 0;
                if(tmp.matches("(\\n\\n)")){
                    silenceSample = samplerate*4;
                }
                else if(tmp.matches(".[\\?\\!\\.\\n]")){
                    silenceSample = (int)(samplerate*0.5);
                }
                else if(tmp.matches(".(\\,)")){
                    silenceSample = (int)(samplerate*0.125);
                }
                sentences.add(
                        Sentence.newBuilder()
                                .setText(text)
                                .setSilenceSample(silenceSample).build());
            }

            if (request.getAudioEncoding() == AudioEncoding.PCM) {
                speakPcmStream(request.getSpeaker(), sentences, responseObserver, client);
            } else if (request.getAudioEncoding() == AudioEncoding.WAV) {
                speakWavStream(request.getSpeaker(), sentences, responseObserver, client);
            } else {
                responseObserver.onError(
                        getGrpcException(
                                CommonCode.TTS_ERR_SVC_WRONG_AUDIO_ENCODING,
                                CommonCode.TTS_ERR_MSG_WRONG_AUDIO_ENCODING)
                );
                logger.info("{}/finished/wrong audio encoding/{}", rpcName, jobId);
                putJobStatus(jobId, CommonCode.JOB_STATUS.error);
                return;
            }

            responseObserver.onCompleted();
            putJobStatus(jobId, CommonCode.JOB_STATUS.completed);

        } catch (Exception e) {
            logger.error("{}: download wave file error. {}", rpcName, e.getMessage());
            putJobStatus(jobId, CommonCode.JOB_STATUS.error);
            responseObserver.onError(
                    getGrpcException(CommonCode.TTS_ERR_SVC_ERROR, e.getMessage())
            );//QQQ
        } finally{

        }
    }

    @Override
    public void speakWavCustom(TtsCustomRequest request, StreamObserver<TtsMediaResponse> responseObserver) {
        String rpcName = "speakWavCustom";
        //set job id
        String jobId = Utils.makeJobId();
        logger.info("{}/{},{}/{}", rpcName, request.getSpeaker(), request.getSentencesList(), jobId);
        putJobStatus(jobId, CommonCode.JOB_STATUS.started);

        if(request.getSentencesCount() == 0){
            responseObserver.onError(
                    getGrpcException(CommonCode.TTS_ERR_SVC_ERROR, "Empty string requested.")
            );
            logger.info("{}/finished/empty request/{}", rpcName, jobId);
            putJobStatus(jobId, CommonCode.JOB_STATUS.error);
            return;
        }

        if(request.getSpeaker() >= maxSpeaker){
            logger.info("{}/finished/{}/{}", rpcName, "exceeded maxspeaker", jobId);
            putJobStatus(jobId, CommonCode.JOB_STATUS.error);
            responseObserver.onError(
                    getGrpcException(
                            CommonCode.TTS_ERR_SVC_EXCEEDED_SPEAKER,
                            CommonCode.TTS_ERR_MSG_SVC_EXCEEDED_SPEAKER)
            );
            return;
        }

        putJobStatus(jobId, CommonCode.JOB_STATUS.running);

        try {
            if (request.getAudioEncoding() == AudioEncoding.PCM) {
                speakPcmStream(request.getSpeaker(), request.getSentencesList(), responseObserver, client);
            } else if (request.getAudioEncoding() == AudioEncoding.WAV) {
                speakWavStream(request.getSpeaker(), request.getSentencesList(), responseObserver, client);
            } else {
                responseObserver.onError(
                        getGrpcException(
                                CommonCode.TTS_ERR_SVC_WRONG_AUDIO_ENCODING,
                                CommonCode.TTS_ERR_MSG_WRONG_AUDIO_ENCODING)
                );
                logger.info("{}/finished/wrong audio encoding/{}", rpcName, jobId);
                putJobStatus(jobId, CommonCode.JOB_STATUS.error);
                return;
            }

            responseObserver.onCompleted();
            putJobStatus(jobId, CommonCode.JOB_STATUS.completed);

        } catch (Exception e) {
            logger.error("{}: download wave file error. {}", rpcName, e.getMessage());
            putJobStatus(jobId, CommonCode.JOB_STATUS.error);
            responseObserver.onError(
                    getGrpcException(CommonCode.TTS_ERR_SVC_ERROR, e.getMessage())
            );//QQQ
        } finally{

        }
    }

    private void speakPcmStream(int speaker, List<Sentence> sentences, StreamObserver<TtsMediaResponse> responseObserver, TTSClient client) throws Exception {
        PipedOutputStream outputStream = new PipedOutputStream();
        PipedInputStream inputStream = new PipedInputStream(outputStream);

        client.doTtsPcm(speaker, sentences, outputStream);
        byte[] buff = new byte[1024 * 32];
        int len = 0;
        while( (len = inputStream.read(buff, 0, buff.length) ) > -1){
            responseObserver.onNext(
                    TtsMediaResponse.newBuilder().setMediaData(ByteString.copyFrom(buff, 0, len)).build()
            );
        }
    }

    private void speakWavStream(int speaker, List<Sentence> sentences, StreamObserver<TtsMediaResponse> responseObserver, TTSClient client) throws Exception {
        PipedOutputStream outputStream = new PipedOutputStream();
        PipedInputStream inputStream = new PipedInputStream(outputStream);

        client.doTtsWav(speaker, sentences, outputStream);
        byte[] buff = new byte[1024 * 64];
        int len = 0;
        while( (len = inputStream.read(buff, 0, buff.length) ) > -1){
            responseObserver.onNext(
                    TtsMediaResponse.newBuilder().setMediaData(ByteString.copyFrom(buff, 0, len)).build()
            );
        }
    }

    private void putJobStatus(String jobId, CommonCode.JOB_STATUS status){
//        JobStatus stat = new JobStatus(jobId, status);
//        mapJobStatus.put(jobId, stat);
        statusManager.putJobStatus(jobId,status);
    }

    private StatusRuntimeException getGrpcException(int errCode, String description){
        StatusRuntimeException ex = Status.INTERNAL.withDescription( description )
                .withCause( new TtsException(errCode, description) )
                .asRuntimeException();
        return ex;
    }
}
