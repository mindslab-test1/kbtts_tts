package maum.brain.tts.server;

import maum.brain.tts.CommonCode;
import maum.brain.tts.client.data.JobStatus;
import maum.brain.tts.utils.Utils;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class JobStatusManager {
    private Logger logger = LoggerFactory.getLogger(NgTtsServer.class);

    private ConcurrentHashMap<String, JobStatus> mapJobStatus = new ConcurrentHashMap<>();
    private static final long expMilis = 300 * 1000; //600 sec

    @Value("${tts.storage.path}")
    private String storageRoot;

    public JobStatusManager(){

    }

    public void putJobStatus(String jobId, CommonCode.JOB_STATUS status){
        JobStatus stat = new JobStatus(jobId, status);
//        synchronized (mapJobStatus) {
            mapJobStatus.put(jobId, stat);
//        }
    }

    public CommonCode.JOB_STATUS getJobStatus(String jobId) {
        JobStatus stat = null;
//        synchronized (mapJobStatus) {
            stat = mapJobStatus.get(jobId);
//        }
        if (stat == null) {
            return CommonCode.JOB_STATUS.notexist;
        }
        return stat.getStatus();
    }

//    public void changeStarted(String jobId){
//        putJobStatus(jobId, CommonCode.JOB_STATUS.started);
//    }
//
//    public void changeRunning(String jobId){
//        putJobStatus(jobId, CommonCode.JOB_STATUS.running);
//    }

    @Scheduled(fixedRate = expMilis) //300 sec.
    private void doExpiry(){
        logger.debug("doExpiry/remove old Job status");
        Set<String> keys = mapJobStatus.keySet();
        if(keys.size() == 0){
            return;
        }
        long curr = System.currentTimeMillis();
//        synchronized (mapJobStatus) {
            for (String key : keys) {

                JobStatus stat = mapJobStatus.get(key);
                if (curr - stat.getTimestamp() > expMilis) {
                    mapJobStatus.remove(key);
                    String outFile = Utils.makeWaveFilePath(storageRoot, key);
                    FileUtils.deleteQuietly(new File(outFile));//
                }
            }
//        }
        logger.debug("doExpiry/finished");
    }
}
