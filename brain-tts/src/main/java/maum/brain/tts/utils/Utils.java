package maum.brain.tts.utils;

import java.util.Random;
import java.util.UUID;

public class Utils {

    public static String makeJobId(){
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }

    public static String makeWaveFilePath(String root, String jobId) {
        return root+"/out/"+jobId+".wav";
    }

    public static int makeRandomId(){
        Random random = new Random();
        int rnd = random.nextInt(99999999);
        return rnd;
    }
}
