package maum.brain.tts.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class NewWaveWriter {
    private ByteArrayOutputStream outStream = new ByteArrayOutputStream();

    private int sampleRate;
    private int nChannels;
    private int sampleBits;

    private int nByteWritten;
    //        private OutputStream outStream;
    private int frameLength;

    //public NewWaveWriter(int sampleRate, int frameLength, OutputStream outputStream) throws IOException {
    public NewWaveWriter(int sampleRate, int frameLength) throws IOException {
        this.sampleRate = sampleRate;
//            this.nChannels = 2;
        this.nChannels = 1;
        this.sampleBits = 16;
        this.frameLength = frameLength;

        this.nByteWritten = 0;
//            this.outStream = outputStream;

        this.outStream.write(new byte[44]);
    }

    public void write(byte[] src, int offset, int length) throws IOException {
        if (offset > length) {
            throw new IndexOutOfBoundsException(String.format("offset %d is greater than length %d", offset, length));
        }
        for (int i = offset; i < length; i+=4) {
            writeUnsignedShortLE(src[i], src[i+1]);
            writeUnsignedShortLE(src[i + 2], src[i + 3]);
            nByteWritten += 4;
        }
    }

    public byte[] getByteBuffer() throws IOException {
        byte[]result = outStream.toByteArray();
        writeWaveHeader(result);
        return result;
    }

    private void writeWaveHeader(byte[]file) throws IOException {
        int bytesPerSec = (sampleBits + 7) / 8;

        int position = 0;
        position = setValue(file, position, "RIFF");
//            position = setValue(file, position, nByteWritten + 36);
        position = setValue(file, position, frameLength + 36);
        position = setValue(file, position, "WAVE");
        position = setValue(file, position, "fmt ");
        position = setValue(file, position, (int)16);
        position = setValue(file, position, (short) 1);
        position = setValue(file, position, (short) nChannels);
        position = setValue(file, position, sampleRate);
        position = setValue(file, position, sampleRate * nChannels * bytesPerSec);
        position = setValue(file, position, (short) (nChannels * bytesPerSec));
        position = setValue(file, position, (short) sampleBits);
        position = setValue(file, position, "data");
//            position = setValue(file, position, nByteWritten);
        position = setValue(file, position, frameLength);
    }

    private void writeUnsignedShortLE(byte sample1, byte sample2)
            throws IOException {
        outStream.write(sample1);
        outStream.write(sample2);
    }

    public static int setValue(byte[]buffer, int position, String value) {
        for (int i = 0; i< value.length(); i++) {
            buffer[position + i] = (byte)value.charAt(i);
        }
        return position + value.length();
    }
    public static int setValue(byte[]buffer, int position, int value) {
        buffer[position + 3] = (byte)(value>>24);
        buffer[position + 2] = (byte)(value>>16);
        buffer[position + 1] = (byte)(value>>8);
        buffer[position + 0] = (byte)(value);
        return position + 4;
    }
    public static int setValue(byte[]buffer, int position, short value) {
        buffer[position + 1] = (byte)(value>>8);
        buffer[position] = (byte)value;
        return position + 2;
    }
}
