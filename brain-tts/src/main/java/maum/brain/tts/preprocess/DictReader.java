package maum.brain.tts.preprocess;
import maum.brain.tts.client.TTSClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

public class DictReader {
    private String result = "";
    private String propFileName = "stub.properties";
    private InputStream inputStream;
    private Logger logger;
    private HashMap<String, String> dictionary;

    // TODO set constructor default logger
    public DictReader() throws IOException {
        this.logger = null;
    }

    public DictReader(Logger logger) throws IOException  {
        this.logger = logger;
    }

    public boolean setProp(String propFileName) throws IOException{
        this.propFileName = propFileName;
        return this.loadValues();
    }

    public HashMap<String, String> getDictionary(){
        return dictionary;
    }

    private boolean loadValues() throws IOException{
        try{
            dictionary = new HashMap<String, String>();

            Properties prop = new Properties();

            inputStream = new FileInputStream(propFileName);
//            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if(inputStream != null){
                prop.load(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
            } else {
                throw new FileNotFoundException("dictionary not found: " + propFileName);
            }

            for (final String key: prop.stringPropertyNames())
                dictionary.put(key, prop.getProperty(key));
            return true;
        } catch (Exception e){
            logger.error("Exception: " + e);

            return false;
        } finally {
            inputStream.close();
        }
    }

    public static void main(String[] args){
        try {
            DictReader testDictReader = new DictReader();
            // property file read test
            String propFileName = "stub.properties";
            if(!testDictReader.setProp(propFileName)){
                throw new FileNotFoundException("dictionary not found: " + propFileName);
            }
            HashMap<String, String> dict = testDictReader.getDictionary();
            for(Map.Entry<String, String> entry : dict.entrySet())
                System.out.println(entry.getKey() + " = " + entry.getValue());
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }
}
