package maum.brain.tts.preprocess;

import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class PreProcessNone implements IPreProcess {
    public ArrayList<String> preProcessText(String text) {
        ArrayList<String> resultStringLst = new ArrayList<>();
        resultStringLst.add(text);
        return resultStringLst;
    }
}
