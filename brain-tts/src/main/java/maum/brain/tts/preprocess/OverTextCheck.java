package maum.brain.tts.preprocess;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class OverTextCheck {

    @Value("${tts.splitlength}")
    private int maxlength;

    public OverTextCheck() {
    }

    public OverTextCheck(int maxlength) {
        this.maxlength = maxlength;
    }

    public ArrayList<String> overCheck(String text) throws IOException {
        ArrayList<String> retStringLst = new ArrayList<>();
//        String test="";
//        Pattern pattern = Pattern.compile("\n");
//        Matcher matcher = pattern.matcher(text);
//        while (matcher.find()){
//            int index = text.indexOf(matcher.group());
//            System.out.println(index);
//             test = text.substring(text.indexOf(index-1,index));
//            if(test.matches("([^\\.\\n])")){
//                test=test+".";
//            }
//        }

//        text = text.replace(matcher.group(),matcher.group()+test);
        if(text.contains("\n\n")) {
            ArrayList<String> newLineSplitLst= new ArrayList<>();
            String retText = text.replaceAll("\n\n","$0|");
            String[] retTextArray = retText.split("\\|");
            for(String tmp : retTextArray){
                if(tmp.length()>1){
                    newLineSplitLst.add(tmp);
                }
            }for(String newLineCutStr : newLineSplitLst) {
//                retStringLst = cutDoubleNewLine(newLineCutStr);
                if (newLineCutStr.length() >= maxlength) {
                    ArrayList<String> cutPunctuationLst = cutPunctuation(newLineCutStr);
                    for (String cutPunctuationStr : cutPunctuationLst) {
                        if (cutPunctuationStr.length() >= maxlength) {
                            ArrayList<String> cutCommaLst = cutComma(cutPunctuationStr);
                            for (String cutCommaStr : cutCommaLst) {
                                if (cutCommaStr.length() >= maxlength) {
                                    ArrayList<String> cutPronunLst = cutPronun(cutCommaStr);
                                    for (String cutPronunStr : cutPronunLst) {
                                        if (cutPronunStr.length() >= maxlength) {
                                            ArrayList<String> cutSpaceLst = cutSpace(cutPronunStr);
                                            for (String cutSpaceStr : cutSpaceLst) {
                                                if (cutSpaceStr.length() >= maxlength) {
                                                    ArrayList<String> cutForceLst = cutForce(cutSpaceStr);
                                                    retStringLst.addAll(cutForceLst);
                                                } else {
                                                    retStringLst.add(cutSpaceStr);
                                                }
                                            }
                                        } else {
                                            retStringLst.add(cutPronunStr);
                                        }
                                    }
                                } else {
                                    retStringLst.add(cutCommaStr);
                                }
                            }
                        } else {
                            retStringLst.add(cutPunctuationStr);
                        }
                    }
                }else {
                    retStringLst.add(newLineCutStr);
                }
            }
        }else{
            retStringLst = cutDoubleNewLine(text);
        }

        return retStringLst;
    }

    private ArrayList<String> cutDoubleNewLine(String input) throws IOException{
        ArrayList<String> retStringLst = new ArrayList<>();
        if(input.length()>=maxlength) {
            ArrayList<String> cutPunctuationLst = cutPunctuation(input);
            for (String cutPunctuationStr : cutPunctuationLst) {
                if (cutPunctuationStr.length() >= maxlength) {
                    ArrayList<String> cutCommaLst = cutComma(cutPunctuationStr);
                    for (String cutCommaStr : cutCommaLst) {
                        if (cutCommaStr.length() >= maxlength) {
                            ArrayList<String> cutPronunLst = cutPronun(cutCommaStr);
                            for (String cutPronunStr : cutPronunLst) {
                                if (cutPronunStr.length() >= maxlength) {
                                    ArrayList<String> cutSpaceLst = cutSpace(cutPronunStr);
                                    for (String cutSpaceStr : cutSpaceLst) {
                                        if (cutSpaceStr.length() >= maxlength) {
                                            ArrayList<String> cutForceLst = cutForce(cutSpaceStr);
                                            retStringLst.addAll(cutForceLst);
                                        } else {
                                            retStringLst.add(cutSpaceStr);
                                        }
                                    }
                                } else {
                                    retStringLst.add(cutPronunStr);
                                }
                            }
                        } else retStringLst.add(cutCommaStr);
                    }
                } else {
                    retStringLst.add(cutPunctuationStr);
                }
            }
        } else{
            retStringLst.add(input);
        }
        return retStringLst;
    }
    private ArrayList<String> cutPunctuation(String input) {
        ArrayList<String> retStringLst = new ArrayList<>();
        ArrayList<String> resultLst = new ArrayList<>();

        String changeText = input.replaceAll("([\\.\\?\\!\\n]+)","$1|");
        String[] textArray = changeText.split("\\|");

        String addText = "";
        for (String tmp:
                textArray
        ) {
            if(tmp.length()>1) {// TODO addall
                retStringLst.add(tmp);
            }
        }
        for (int i=0; i<retStringLst.size();i++){
            addText += retStringLst.get(i);
//            if(addText.length()>maxlength/2){
                resultLst.add(addText);
                addText="";
//            }
        }
        if(addText.length()>1){
            resultLst.add(addText);
        }
        return resultLst;
    }

    private ArrayList<String> cutComma(String input) throws IOException{
//        ArrayList<String> retStringLst = new ArrayList<>();
//
//        String changeText = input.replaceAll("(\\,+)", "$1|");
//        String[] textArray = changeText.split("\\|");
//
//        String addText="";
        ArrayList<String> resultLst = new ArrayList<>();
        resultLst.add(input);
//
//        for (String tmp:textArray) {
//            if (tmp.length() > 1) {
//                retStringLst.add(tmp);
//            }
//        }
//
//        for(int i=0; i<retStringLst.size();i++){
//            addText += retStringLst.get(i);
//
//            if(addText.length()>maxlength/1.5){
//                resultLst.add(addText);
//                addText ="";
//            }
//        }
//        if(addText.length()>1){
//            resultLst.add(addText);
//        }
        return resultLst;
    }
    private ArrayList<String> cutPronun(String input) throws IOException{
        ArrayList<String> retStringLst = new ArrayList<>();
        ArrayList<String> resultLst = new ArrayList<>();
        String addText="";

        String changeText = input.replaceAll("[\\}]\\s\\{(\\s|\\w|\\d)*[\\}]", "$0|");
        String[] textArray = changeText.split("\\|");
        for (String tmp : textArray) {
            if(tmp.length()>=maxlength){
                tmp = tmp.replaceAll("[\\{](\\s|\\w|\\d)*[\\}]","$0|");
                Pattern pattern = Pattern.compile("(\\}\\|\\s\\{(\\s|\\w|\\d)*\\})\\|");
                Matcher matcher = pattern.matcher(tmp);
                String noChnageStr="";
                while (matcher.find()){
                    noChnageStr = matcher.group().replaceAll("\\|","");
                    tmp = tmp.replace(matcher.group(),noChnageStr);
                }

                String[] tmpArray = tmp.split("\\|");
                for(String oneTmp : tmpArray){
                    retStringLst.add(oneTmp);
                }
            }else {
                retStringLst.add(tmp);
            }
        }


        for(int i=0; i<retStringLst.size();i++){
            addText += retStringLst.get(i);

            if(addText.length()>maxlength*2/3){
                resultLst.add(addText);
                addText ="";
            }
        }
        if(addText.length()>1) {
            resultLst.add(addText);
        }

        return resultLst;
    }
    private ArrayList<String> cutSpace(String input) throws IOException{
        ArrayList<String> retStringLst = new ArrayList<>();

        String changeText = input.replaceAll("( |\\,+)", "$1|");
        String[] textArray = changeText.split("\\|");

        String addText="";
        ArrayList<String> resultLst = new ArrayList<>();

        for (String tmp : textArray) {
            retStringLst.add(tmp);
        }
        for(int i=0; i<retStringLst.size();i++){
            addText += retStringLst.get(i);

            if(addText.length()>maxlength/2){
                addText = addText.substring(0,addText.length()-1);
                String tmp = addText.substring(addText.length());
                resultLst.add(addText+",,,");
                addText ="";
            }
        }
        //non-negative Error 텍스트 값이 없을 경우를 처리 (addText = "")
          if(addText.length()>1) {
            resultLst.add(addText);
        }
        return resultLst;
    }
    private ArrayList<String> cutForce(String input) throws IOException{
        ArrayList<String> resultLst =new ArrayList<>();
        String[] textArray = new String[(input.length()/(maxlength/2))+1];
        int start = 0;
        int end = maxlength/2;
        for(int i=0; i<=input.length()/(maxlength/2);i++){
            String tmp;
            System.out.println(i);

            if(end>input.length()){
                tmp = input.substring(start);
                textArray[i]=tmp;
            }else {
                tmp = input.substring(start, end);
                textArray[i] = tmp;
                start = start+maxlength/2;
                end =start+maxlength/2;

            }
        }
        for(String tmp : textArray){
            resultLst.add(tmp);
        }
        return resultLst;
    }

}