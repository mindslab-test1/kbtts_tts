package maum.brain.tts.preprocess;

import maum.brain.tts.client.G2PClient;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Primary
@ConditionalOnProperty(name = "tts.lang", havingValue = "en_US")
public class PreProcessEng implements IPreProcess {

    // 패턴확인용 regex
    private String ordinalCheck = "(1st|2nd|3rd|[0-9]+th)";
    private String numCheck = "([+-]?\\d[\\d,]*)([\\.\\d]*)";
    private String metricsNum = "(([+-]?\\d[\\d,]*)([\\.\\d]*)(?!th)(?!rd)(?!st)(?!nd))";
    private String metricsCheck = "[a-zA-Z|%]+";
    private HashMap<String, String> dictWord;
    //one~nine
    private HashMap<String, String> dictNum;
    private HashMap<String, String> dictMetrics;
    private HashMap<Integer, String> dictDigit;
    //engNum2dict = one~eleven, fourty ...
    private HashMap<String, String> dictNumOrdinal;
    private HashMap<String, String> dictNumChange;
    //calender date / month
    private HashMap<String, String> dictCalDate;
    private HashMap<String, String> dictCalMonth;

//    private HashMap<String, String> mappingDict;
//    private HashMap<String, String> mappingDoubleDict;

    @Value("${tts.resources.preprocess.root}")
    private String dictRoot;
    @Value("${tts.resources.preprocess.eng.base}")
    private String dictBase;

    private Logger logger;

    private DictReader dictReader;
    static public NumberToEng.AbstractProcessor processor;

    @Autowired
    private G2PClient g2p;
    @Autowired
    private OverTextCheck overTextCheck;

    public PreProcessEng() {
    }

    @PostConstruct
    public void init() throws IOException {
        this.dictReader = new DictReader();
        this.dictReader.setProp(dictRoot + dictBase);
        this.readDictionaries();
    }

    public PreProcessEng(int maxlength, String propFileName, String g2pAddress) {
        this.logger = null;
        g2p = new G2PClient(g2pAddress);
        overTextCheck = new OverTextCheck(maxlength);
        this.dictRoot = "";
        this.dictBase = propFileName;
    }

    public PreProcessEng(int maxlength, String propFileName, Logger logger, String g2pAddress) {
        this.logger = logger;
        g2p = new G2PClient(g2pAddress);
        overTextCheck = new OverTextCheck(maxlength);
        this.dictRoot = "";
        this.dictBase = propFileName;
    }

    public PreProcessEng(int maxlength, String propFileRoot, String propFileBase, Logger logger, String g2pAddress) {
        this.logger = logger;
        g2p = new G2PClient(g2pAddress);
        overTextCheck = new OverTextCheck(maxlength);
        this.dictRoot = propFileRoot;
        this.dictBase = propFileBase;
    }

    private void readDictionaries() {
        try {
            Iterator<Map.Entry<String, String>> it = this.dictReader.getDictionary().entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> entry = it.next();
                // TODO - upgrade, make custom dictionary addition more easy
                switch (entry.getKey()) {
                    case "eng.word":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictWord = dictReader.getDictionary();
                        break;
                    case "eng.num":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictNum = dictReader.getDictionary();
                        break;
                    case "eng.metrics":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictMetrics = dictReader.getDictionary();
                        break;
                    case "eng.num_ordinal":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictNumOrdinal = dictReader.getDictionary();
                        break;
                    case "eng.cal.date":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictCalDate = dictReader.getDictionary();
                        break;
                    case "eng.cal.month":
                        dictReader.setProp(dictRoot + entry.getValue());
                        dictCalMonth = dictReader.getDictionary();
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public ArrayList<String> preProcessText(String text) throws IOException {
        String processed = text.trim();
        ArrayList<String> resultStringLst;
        processed = normHtmlTag(processed);
        processed = normDollar(processed);
        processed = normWord(processed);
        processed = normCalenderMDY(processed);
        processed = normCalenderYMD(processed);
        processed = normDeleteDash(processed);
        processed = normMinusDash(processed);
        processed = normToDash(processed);
        processed = normNum(processed);
        processed = normOrdinalNum(processed);
        processed = normAfter(processed);
        resultStringLst = overTextCheck.overCheck(processed);
        resultStringLst = changePronun(resultStringLst);
        return resultStringLst;
    }

    //input text 에 html 태그가 들어왔을 때 제거
    private String normHtmlTag(String text) {
        String retText = text;
        Pattern pattern = Pattern.compile("(\\<[^\\<\\>]*\\>)");
        Matcher matcher = pattern.matcher(retText);
        while (matcher.find()) {
            retText = retText.replace(matcher.group(), " ");
        }
        return retText;
    }

    private String normDollar(String text) {
        String retText = text;
        Pattern pText = Pattern.compile("\\-\\$\\d+( billion|billion| million|million|thousand| thousand|trillion| trillion)");
        Matcher pMat = pText.matcher(retText);
        while (pMat.find()) {
            String tmp = pMat.group();
            tmp = tmp.replace("$", "");
            tmp = tmp.replace("-", " minus ");
            tmp = tmp + "dollars";
            retText = retText.replace(pMat.group(), tmp);
        }
        pText = Pattern.compile("\\d+\\-\\$\\d+\\.\\d{2}");
        pMat = pText.matcher(retText);
        while (pMat.find()) {
            String tmp = pMat.group();
            tmp = tmp.replace("$", "");
            tmp = tmp.replace("-", " to ");
            tmp = tmp.replace(".", "dollars ");
            tmp = tmp + " cents ";
            retText = retText.replace(pMat.group(), tmp);
        }

        pText = Pattern.compile("\\d+\\-\\$\\d+");
        pMat = pText.matcher(retText);
        while (pMat.find()) {
            String tmp = pMat.group();
            tmp = tmp.replace("$", "");
            tmp = tmp.replace("-", " to ");
            tmp = tmp + " dollars";
            retText = retText.replace(pMat.group(), tmp);
        }
        //dollars / cents  치환
        pText = Pattern.compile("\\-\\$\\d+\\.\\d{2}|\\$\\d+\\.\\d{2}");
        pMat = pText.matcher(retText);
        while (pMat.find()) {
            String tmp = pMat.group();
            tmp = tmp.replace("$", "");
            tmp = tmp.replace("-", " minus ");
            tmp = tmp.replace(".", "dollars ");
            tmp = tmp + " cents ";
            retText = retText.replace(pMat.group(), tmp);
        }

        pText = Pattern.compile("\\-\\$\\d+|\\$\\d+");
        pMat = pText.matcher(retText);
        while (pMat.find()) {
            String tmp = pMat.group();
            tmp = tmp.replace("$", "");
            tmp = tmp.replace("-", " minus ");
            tmp = tmp + " dollars";
            retText = retText.replace(pMat.group(), tmp);
        }
        return retText;
    }


    private String normWord(String text) {
        text = text.replaceAll(String.format("\\b%s\\.", "mr"), "mister ");
        text = text.replaceAll(String.format("\\b%s\\.", "Mr"), "mister ");
        text = text.replaceAll(String.format("\\b%s\\.", "mrs"), "misses ");
        text = text.replaceAll(String.format("\\b%s\\.", "Mrs"), "misses ");
        text = text.replaceAll(String.format("\\b%s\\.", "Ms"), "miss ");
        String retText = text;
        List<String> subStrList = new ArrayList<>();
        Pattern pText = Pattern.compile("\\b[a-zA-Z]+\\b");
        Matcher matcher = pText.matcher(text);
        while (matcher.find()) {
            subStrList.add(matcher.group());
        }
        for (String wordSubStr : subStrList) {
            if (dictWord.get(wordSubStr) != null)
//                retText = retText.replaceFirst(wordSubStr, engWordDict.get(wordSubStr));
                retText = retText.replaceAll(String.format("\\b%s\\b", wordSubStr), dictWord.get(wordSubStr));
        }

        retText = retText.replace("$", " dollars ");
        retText = retText.replace("£", " pounds ");
        return retText;
    }

    private String normNum(String text) {

        String retText = text;

        Pattern metricsPattern = Pattern.compile("\\b" + metricsNum + metricsCheck);

        Matcher metricsMatcher = metricsPattern.matcher(retText);

        while (metricsMatcher.find()) {
            String tmp = normMetrics(metricsMatcher.group());
            retText = retText.replaceFirst(metricsMatcher.group(), tmp);
        }

        Pattern numberPattern = Pattern.compile(numCheck);
        Matcher numberMatcher = numberPattern.matcher(retText);
        String matcherGroup = "";
        ArrayList<String> beforStr = new ArrayList<>();
        String afterStr = "";
        int i = 0;
        //발음기호로 들어온 input text를 숫자변경 전과 후로 나눔
        Pattern beforePat = Pattern.compile("[\\{](\\w|\\d|\\s)*[\\}]");
        Matcher beforeMat = beforePat.matcher(retText);

        while (beforeMat.find()) {
            beforStr.add(i, beforeMat.group());
            i++;
        }

        i = 0;

        while (numberMatcher.find()) {

            matcherGroup = numberMatcher.group();
            processor = new NumberToEng.DefaultProcessor();
            int floatCheck = matcherGroup.length() - matcherGroup.replace(".", "").length();
            if (1 < floatCheck) {
                retText = retText.replace(matcherGroup, normSimpleNum(matcherGroup.replace(".", " point ")));
            } else if (1 == floatCheck) {
                String intNum = matcherGroup.split("\\.")[0];
                if (matcherGroup.split("\\.").length > 1) {
                    String fltNum = matcherGroup.split("\\.")[1];
                    long checkComma = Long.parseLong(intNum.replace(",", ""));
                    intNum = commaToNumber(intNum, checkComma);
                    fltNum = normSimpleNum(fltNum);
                    retText = retText.replace(matcherGroup, intNum + " point " + fltNum);
                } else {
                    intNum = intNum.replace(",", "");
                    intNum = processor.getName(intNum);
                    retText = retText.replace(matcherGroup, intNum);
                }
            } else {
                String checkCommaStr = matcherGroup.replace(",", "");
                long checkComma = Long.parseLong(checkCommaStr);
                String returnText = "";
                returnText = commaToNumber(matcherGroup, checkComma);
                retText = retText.replaceFirst(matcherGroup, returnText);
            }

        }

        Pattern afterPat = Pattern.compile("[\\{](\\w|\\d|\\s)*[\\}]");
        Matcher afterMat = afterPat.matcher(retText);
        while (afterMat.find()) {

            afterStr = afterMat.group();
            retText = retText.replace(afterStr, beforStr.get(i));
            i++;
        }
        return retText;
    }

    private String commaToNumber(String text, long checkComma) {
        String returnText;
        if (String.format("%,d", checkComma).equals(text)) {
            returnText = text.replace(",", "");
            returnText = processor.getName(returnText);
        } else {
            String[] intNumArray = text.split(",");
            String commaSpace = ", ";
            StringBuilder splitText = new StringBuilder();
            for (int i = 0; i < intNumArray.length; i++) {
                splitText.append(processor.getName(intNumArray[i]));
                if (i != intNumArray.length - 1)
                    splitText.append(commaSpace);
            }
            returnText = splitText.toString();
        }
        return returnText;
    }

    private String normOrdinalNum(String text) {
        String retText = text;

        for (Map.Entry<String, String> entry : dictNumOrdinal.entrySet()) {
            retText = retText.replace(entry.getKey(), entry.getValue());
        }

        return retText;
    }

    private String normAfter(String text) {
        String retText = text;/*.replaceAll(",", " ");*/
//        retText = retText.replace(".", " dot ");

        retText = retText.replace("`", "'");
        retText = retText.replace("｀", "'");
        //뉴라인이나 스페이스 여러개는 스페이스 하나로 치환
        retText = retText.replaceAll("-", " ");
        retText = retText.replaceAll("\n+| {2,}", " ");
        retText = retText.replaceAll("\\.{2,}",".");
        retText = retText.replaceAll("(a\\.m\\.)|(A\\.M\\.)|(a\\.m)|(A\\.M)", "AM");
        retText = retText.replaceAll("(p\\.m\\.)|(P\\.M\\.)|(p\\.m)|(P\\.M)", "PM");
        retText = retText.replaceAll("[^a-zA-Z\\!\\'\\(\\)\\,\\-\\.\\:\\;\\? \n\\/]", "");
        retText = retText.trim();
        return retText;
    }


    private ArrayList<String> changePronun(ArrayList<String> text) {
        List<String> sentenceList = g2p.callG2p(text);
        return new ArrayList<>(sentenceList);
    }

    private String normMetrics(String text) {
        String partMetrics = "";
        String partNum = "";
        Pattern pText = Pattern.compile(metricsCheck);
        Matcher pMat = pText.matcher(text);
        if (pMat.find()) {
            partMetrics = pMat.group();
            partNum = text.replace(partMetrics, "");

            for (Map.Entry<String, String> entry : dictMetrics.entrySet()) {
                if (entry.getKey().equals(pMat.group())) {
                    partMetrics = partMetrics.replaceFirst(entry.getKey(), entry.getValue());
                    break;
                }
            }
        }
        return partNum + partMetrics;
    }

    private String normSimpleNum(String text) {
        String retText = text;

        for (Map.Entry<String, String> entry : dictNum.entrySet()) {
            retText = retText.replace(entry.getKey(), " " + entry.getValue() + " ");
        }
        return retText;
    }

    private String normDeleteDash(String text) {
        String retText = text;
        String changeText = "";
        Pattern dashTextPattern = Pattern.compile("(-+[a-zA-Z]+-+)|([a-zA-Z]+-+)|(-+[a-zA-Z]+)");
        Matcher dashTextMatcher = dashTextPattern.matcher(retText);
        while (dashTextMatcher.find()) {
            changeText = dashTextMatcher.group().replace("-", " ");
            retText = retText.replace(dashTextMatcher.group(), changeText);
        }
        return retText;
    }

    private String normMinusDash(String text) {
        String retText = text;
        String changeText = "";
        Pattern dashTextPattern = Pattern.compile("\\d+ - \\d+");
        Matcher dashTextMatcher = dashTextPattern.matcher(retText);
        while (dashTextMatcher.find()) {
            changeText = dashTextMatcher.group().replace("-", " minus ");
            retText = retText.replace(dashTextMatcher.group(), changeText);
        }
        return retText;
    }

    private String normToDash(String text) {
        String retText = text;
        String changeText = "";
        Pattern dashTextPattern = Pattern.compile("\\d+-\\d+");
        Matcher dashTextMatcher = dashTextPattern.matcher(retText);
        while (dashTextMatcher.find()) {
            changeText = dashTextMatcher.group().replace("-", " to ");
            retText = retText.replace(dashTextMatcher.group(), changeText);
        }
        return retText;
    }

    private String normCalenderMDY(String text) {
        String retText = text;
        String changeDateText = "";
        String changeMonthText = "";

        Pattern engCalenderPattern = Pattern.compile("((0[1-9]|1[0-2]|\\b[1-9])(\\.|-|\\/|\\_)((0[1-9]|[12]\\d|3[01])|\\b[1-9])(\\.|-|\\/|\\_)[12]\\d{3})");
        Matcher engCalenderMatcher = engCalenderPattern.matcher(retText);
        if (engCalenderMatcher.find()) {
            changeMonthText = engCalenderMatcher.group().split("(\\.|-|\\/|\\_)")[0];
            changeDateText = engCalenderMatcher.group().split("(\\.|-|\\/|\\_)")[1];
            retText = changeCalenderText(engCalenderMatcher.group().split("(\\.|-|\\/|\\_)")[2], changeMonthText, changeDateText, false);
        }
        return retText;
    }

    private String normCalenderYMD(String text) {

        String retText = text;
        String changeDateText = "";
        String changeMonthText = "";

        Pattern engCalenderPattern = Pattern.compile("([12]\\d{3}(\\.|-|\\/|\\_)(0[1-9]|1[0-2]|[1-9])(\\.|-|\\/|\\_)((0[1-9]|[12]\\d|3[01])|[1-9]\\b))");
        Matcher engCalenderMatcher = engCalenderPattern.matcher(retText);
        if (engCalenderMatcher.find()) {
            changeMonthText = engCalenderMatcher.group().split("(\\.|-|\\/|\\_)")[1];
            changeDateText = engCalenderMatcher.group().split("(\\.|-|\\/|\\_)")[2];
            retText = changeCalenderText(engCalenderMatcher.group().split("(\\.|-|\\/|\\_)")[0], changeMonthText, changeDateText, true);
        }
        return retText;
    }

    private String changeCalenderText(String yearText, String montText, String dateText, boolean YMD) {
        String retText = "";
        for (Map.Entry<String, String> entry : dictCalMonth.entrySet()) {
            montText = montText.replace(entry.getKey(), entry.getValue());
        }
        for (Map.Entry<String, String> entry : dictCalDate.entrySet()) {
            dateText = dateText.replace(entry.getKey(), entry.getValue());
        }
        if (YMD)
            retText = yearText + " " + montText + " " + dateText;
        else
            retText = montText + " " + dateText + " " + yearText;

        return retText;
    }

    public static void main(String[] args) {
        try {
            PreProcessEng preProcessEngTest = new PreProcessEng(150, "tts.eng.dict.list.properties", "127.0.0.1:19001");
            preProcessEngTest.init();
//            System.out.println(preProcessEngTest.preProcessTest(""));
        } catch (Exception e) {
            System.out.println("Exception: " + e);

        }

    }
}
