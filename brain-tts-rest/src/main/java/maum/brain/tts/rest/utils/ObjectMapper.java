package maum.brain.tts.rest.utils;

import maum.brain.tts.AudioEncoding;
import maum.brain.tts.rest.CommonCode;
import maum.brain.tts.rest.exception.TtsRestException;

public class ObjectMapper {

    public static AudioEncoding convertAudioEncoding(CommonCode.AudioEncoding audioEncoding) throws TtsRestException {
        AudioEncoding result;
        switch(audioEncoding){
            case WAV:
                result = AudioEncoding.WAV;
                break;
            case PCM:
                result = AudioEncoding.PCM;
                break;
            default:
                throw new TtsRestException(
                        CommonCode.TTS_ERR_NOT_SUPPORTED_AUDIOENCODING_ERROR,
                        CommonCode.TTS_ERR_MSG_NOT_SUPPORTED_AUDIOENCODING_ERROR);
        }
        return result;
    }
}
