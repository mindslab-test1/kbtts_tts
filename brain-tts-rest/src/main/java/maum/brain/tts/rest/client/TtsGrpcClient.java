package maum.brain.tts.rest.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.tts.*;
import maum.brain.tts.rest.CommonCode;
import maum.brain.tts.rest.exception.TtsRestException;
import maum.brain.tts.rest.lookup.GrpcTtsServerLookup;
import maum.brain.tts.rest.lookup.data.GrpcServerInfo;
import maum.brain.tts.rest.utils.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

@Component
public class TtsGrpcClient{

    @Autowired
    private GrpcTtsServerLookup resolver;

    private final long deadline = 300L;

    public ManagedChannel makeChannel(String name) {
        GrpcServerInfo server = resolver.lookup(name);
        return ManagedChannelBuilder.forAddress(server.getIp(), server.getPort())
                .usePlaintext()
                .build();
    }

    public Iterator<TtsMediaResponse> speakBlocking(ManagedChannel channel, int speaker, String text,
                                                    CommonCode.AudioEncoding audioEncoding) throws TtsRestException {
        maum.brain.tts.AudioEncoding ttsAudioEncoding = ObjectMapper.convertAudioEncoding(audioEncoding);
        NgTtsServiceGrpc.NgTtsServiceBlockingStub stub = NgTtsServiceGrpc.newBlockingStub(channel);

        TtsRequest req = TtsRequest.newBuilder()
                .setSpeaker(speaker)
                .setText(text)
                .setAudioEncoding(ttsAudioEncoding)
                .build();

        Iterator<TtsMediaResponse> iter = stub.withDeadlineAfter(deadline, TimeUnit.SECONDS)
                .speakWav(req);

        return iter;
    }


}
