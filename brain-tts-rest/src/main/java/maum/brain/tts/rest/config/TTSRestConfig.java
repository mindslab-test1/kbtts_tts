package maum.brain.tts.rest.config;

import maum.brain.tts.rest.lookup.GrpcTtsServerLookup;
import maum.brain.tts.rest.utils.ConvertAudio;
import maum.brain.tts.rest.utils.FileDownloadUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.concurrent.Executor;

@Configuration
@ComponentScan(basePackages={"maum.brain.tts.rest"})
@EnableScheduling
public class TTSRestConfig {

    @Bean
    public TaskScheduler taskScheduler() {
//        return new ConcurrentTaskScheduler();//single thread
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(10);
        scheduler.setWaitForTasksToCompleteOnShutdown(true);
        return scheduler;
    }

    @Bean
    public FileDownloadUtil fileDownloadUtil(){
        return new FileDownloadUtil();
    }
    @Bean
    public ConvertAudio convertAudio(){
        return new ConvertAudio();
    }
    @Bean
    public Executor taskExecutor() {
        return new SimpleAsyncTaskExecutor();
    }
}

