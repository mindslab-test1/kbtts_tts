package maum.brain.tts.rest.controller.data;

import maum.brain.tts.rest.CommonCode;

import java.io.Serializable;

public class TtsRestRequest implements Serializable {
    private String name;
    private int speaker;
    private String text;
    private CommonCode.AudioEncoding audioEncoding;

    public TtsRestRequest(){

    }

    public TtsRestRequest(String name, int speaker, String text, CommonCode.AudioEncoding audioEncoding){
        this.name = name;
        this.speaker = speaker;
        this.text = text;
        this.audioEncoding = audioEncoding;
    }

    public CommonCode.AudioEncoding getAudioEncoding() { return audioEncoding; }

    public void setAudioEncoding(CommonCode.AudioEncoding audioEncoding) { this.audioEncoding = audioEncoding; }

    public String getName() {  return name; }

    public void setName(String name) { this.name = name; }

    public String getText() {
        return text;
    }

    public void setSpeaker(int speaker) {
        this.speaker = speaker;
    }

    public int getSpeaker() {
        return speaker;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "TtsRestRequest{" +
                "name='" + name + '\'' +
                ", speaker=" + speaker +
                ", text='" + text + '\'' +
                ", audioEncoding=" + audioEncoding +
                '}';
    }
}
