package maum.brain.tts.rest.lookup;

import maum.brain.tts.rest.CommonCode;
import maum.brain.tts.rest.exception.TtsRestException;
import maum.brain.tts.rest.lookup.data.GrpcServerInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;

@Service
public class GrpcTtsServerLookup {

    private Logger logger = LoggerFactory.getLogger(GrpcTtsServerLookup.class);

    @Autowired
    private Environment env;

    @Value("${tts.grpc.names}")
    private String[] names;

    @Value("${tts.grpc.ips}")
    private String[] ips;

    @Value("${tts.grpc.ports}")
    private int[] ports;

    protected HashMap<String, GrpcServerInfo> servers = new HashMap<>();

    public GrpcTtsServerLookup(){

    }

    @PostConstruct
    private void initData() throws TtsRestException {
        if (ips.length != names.length || ports.length != names.length) {
            throw new TtsRestException(CommonCode.TTS_ERR_GRPC_CONFIG, CommonCode.TTS_ERR_MSG_GRPC_CONFIG);
        }
        for (int serverIdx = 0; serverIdx < names.length; serverIdx++) {
            String name = names[serverIdx];
            String ip = ips[serverIdx];
            int port = ports[serverIdx];

            GrpcServerInfo info = new GrpcServerInfo(ip, port);
            servers.put(name, info);
            logger.info("{} servers:{}", name, info);
        }
    }

    public GrpcServerInfo lookup(String name) {
        return servers.get(name);
    }

    public String[] getNames() { return names; }
}
