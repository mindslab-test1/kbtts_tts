package maum.brain.tts.rest.controller;


import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.rest.exception.TtsRestException;
import maum.brain.tts.rest.client.TtsGrpcClient;

import maum.brain.tts.rest.controller.data.TtsRestRequest;
import maum.brain.tts.rest.lookup.GrpcTtsServerLookup;
import maum.brain.tts.rest.utils.ConvertAudio;
import maum.brain.tts.rest.utils.FileDownloadUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import java.nio.channels.WritableByteChannel;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Controller
public class TtsController {

    private Logger logger = LoggerFactory.getLogger(TtsController.class);

    @Autowired
    private GrpcTtsServerLookup resolver;

    @Autowired
    private TtsGrpcClient client;

    @Autowired
    private ConvertAudio convertAudio;

    @Value("${tts.maxlength}")
    private int maxLength;

    @Value("${tts.downloads}")
    private String downloads;

    @RequestMapping("/index")
    public String index(Model model) {
        model.addAttribute("maxLength", maxLength);
        model.addAttribute("grpcNames", resolver.getNames());
        return "thymeleaf/index";
    }

    @RequestMapping("/index_pcm")
    public String index_pcm(Model model) {
        model.addAttribute("maxLength", maxLength);
        model.addAttribute("grpcNames", resolver.getNames());
        return "thymeleaf/index_pcm";
    }

    @RequestMapping(value="/tts/block/speak",method = {RequestMethod.POST}, consumes="application/json")
    public StreamingResponseBody blockingSpeak(@RequestBody TtsRestRequest request, HttpServletResponse response, HttpServletRequest servletRequest) throws Exception{
        logger.info("tts/block/speak/{}",request);
        try {
            ManagedChannel channel = client.makeChannel(request.getName());
            Iterator<TtsMediaResponse> iter = client.speakBlocking(
                    channel, request.getSpeaker(), request.getText(), request.getAudioEncoding());
            File fileDir = new File(downloads);
            if(!fileDir.exists()){
                fileDir.mkdir();
                System.out.println("Make Directory");
            }
            String fileId = servletRequest.getSession().toString();
            fileId = fileId.substring(fileId.indexOf("@")+1);
            File file = new File(downloads+fileId+".wav");
            FileOutputStream fos = new FileOutputStream(file);

            return new StreamingResponseBody() {
                @Override
                public void writeTo (OutputStream out) throws IOException {
                    WritableByteChannel och = Channels.newChannel(out);

                    try {
                        TtsMediaResponse ttsResp = null;

                        while (iter.hasNext()) {
                            ttsResp = iter.next();

                            ByteString data = ttsResp.getMediaData();
                            ByteBuffer buff = data.asReadOnlyByteBuffer();
                            fos.write(ttsResp.getMediaData().toByteArray());
                            och.write(buff);
                        }
                    }finally{
                        logger.info("tts/block/speak/close");
                        och.close();
                        out.close();
                        fos.close();
                        channel.shutdown();
                        convertAudio.deleteFile();
                    }
                }
            };
        }catch(TtsRestException e){
            logger.warn("tts/block/speak/");
            throw e;
        }catch(Exception e){
            logger.warn("tts/block/speak/");
            throw e;
        }
    }


    @RequestMapping(value = "/tts/ttsDwn")
    public ModelAndView fileDownload(HttpServletRequest request, @RequestParam("file") String fileSet, @RequestParam("speed") String speed, @RequestParam("tone") String tone
    ) throws Exception {
        String fileId = request.getSession().toString();
        fileId = fileId.substring(fileId.indexOf("@")+1);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(FileDownloadUtil.class);
        FileDownloadUtil fileDownloadUtil = context.getBean(FileDownloadUtil.class);
        File file = new File(downloads+fileId+".wav");
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
        int sampleRate = (int)audioInputStream.getFormat().getSampleRate();
        String fileName =convertAudio.convert(file,fileSet,speed,tone,sampleRate);

        String fileNameKey=fileName;

        /** 첨부파일 정보 조회 */
        Map<String, Object> fileInfo = null;
        try {
            fileInfo = new HashMap<String, Object>();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fileInfo.put("fileNameKey", fileNameKey);
        fileInfo.put("fileName", fileName);
        fileInfo.put("filePath", downloads);


        return new ModelAndView(fileDownloadUtil, "fileInfo", fileInfo);
    }
}
