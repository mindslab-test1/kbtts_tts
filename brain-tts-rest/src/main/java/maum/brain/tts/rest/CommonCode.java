package maum.brain.tts.rest;

public class CommonCode {

    //Socket related error
    public static int TTS_RES_SUCCESS = 0;
    public static int TTS_ERR_CONN_REFUSED  = 50001;
    public static int TTS_ERR_CONN_TIMEOUT  = 50002;
    public static int TTS_ERR_REQ_TIMEOUT   = 50003;

    public static int TTS_ERR_JOB_STILL_RUNNING = 59001;
    public static int TTS_ERR_JOB_NOT_EXIST     = 59002;

    public static int TTS_ERR_AUTH_VERIFICATION_ERROR   = 40001;
    public static int TTS_ERR_SERVER_NOT_FOUND_ERROR    = 40002;
    public static int TTS_ERR_NOT_SUPPORTED_LANG_ERROR    = 40003;
    public static int TTS_ERR_NOT_SUPPORTED_AUDIOENCODING_ERROR = 40004;
    public static String TTS_ERR_MSG_NOT_SUPPORTED_AUDIOENCODING_ERROR = "Not supported audioEncoding";

    public static int TTS_ERR_GRPC_CONFIG = 60001;
    public static String TTS_ERR_MSG_GRPC_CONFIG = "There is difference between name length and ip or port length.";

    //Service related error
    public static int TTS_ERR_SVC_ERROR = 1;
    public static int TTS_ERR_SVC_EXCEEDED_TEXT_LENGTH = 60001;
    public static String TTS_ERR_MSG_SVC_EXCEEDED_TEXT_LENGTH = "Requested text exceeded max. length.";
    public static String TTS_ERR_MSG_SUCCESS = "Success";

    public enum JOB_STATUS{
        notexist,
        started,
        running,
        completed,
        error
    }

    public enum AudioEncoding{
        WAV,
        PCM
    }
}
