package maum.brain.tts.rest.lookup.data;

import java.io.Serializable;

public class GrpcServerInfo implements Serializable {

    protected String ip;
    protected int port;

    public GrpcServerInfo(){

    }

    public GrpcServerInfo(String ip, int port){
//        super(ip, port);
        this.ip = ip;
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "GrpcServerInfo{" +
                "ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }
}
