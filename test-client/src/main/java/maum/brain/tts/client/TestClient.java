package maum.brain.tts.client;

import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import maum.brain.tts.*;
import maum.common.LangOuterClass;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@ComponentScan(basePackages={"maum.brain.tts"})
public class TestClient implements CommandLineRunner {

//    private static Logger logger = LoggerFactory.getLogger(TestClient.class);
    private CliLogger logger = new CliLogger();

    private String ip;
    private int port;
    private String lang;
    private int speaker;
    private String text;
    private String outfile;
    private boolean download = true;

    public void setPort(int p){
        this.port = p;
    }

    public void setSpeaker(int speaker){
        this.speaker = speaker;
    }

    public static void main(String[] args){
        System.out.println("TestClinet started.");
        SpringApplication.run(TestClient.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("speakWav test...");

        Options options = new Options();
        Option help = new Option( "help", "help" );

        options.addOption(help);
        options.addOption("ip", true, "ip");
        options.addOption("port", true, "server port");
        options.addOption("lang",true,"language type, ko_KR or en_US");
        options.addOption("speaker", true, "speaker index");
        options.addOption("text", true, "text");
        options.addOption("out", true, "out file");
        options.addOption("task", true, "task type. s:speak(non-blocking), c:check status, w:get wav file");
        options.addOption("job", true, "job id, it's for async call");
        options.addOption("token", true, "token, it's for async call");

        try {
            CommandLineParser parser = new DefaultParser();
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);

            if (line.hasOption("help")) {
                TestClient.printUsage(options);
                System.exit(0);
            }

//            Application app = new Application();
            if (!line.hasOption("ip")) {
                TestClient.printUsage(options);
                System.exit(-1);
            }else{
                String addr = line.getOptionValue("ip");
                this.ip = addr;
            }

            if (line.hasOption("port")) {
                String s = line.getOptionValue("port");
                int p = 0;
                if(s.matches("\\d+")){
                    p = Integer.parseInt(s);
                    this.setPort(p);
                }else{
                    logger.error("port parsing error. " + s);
                    TestClient.printUsage(options);
                    System.exit(-1);
                }

            }else{
                TestClient.printUsage(options);
                logger.error("port is not specified.");
                System.exit(-1);
            }

            this.download = true;

            if(!line.hasOption("lang")){
                TestClient.printUsage(options);
                System.exit(-1);
            }
            String lang = line.getOptionValue("lang");
            if(lang.equals("ko_KR") || lang.equals("en_US")){
                this.lang = lang;
            }else{
                TestClient.printUsage(options);
                System.exit(-1);
            }
            if(!line.hasOption("speaker")){
                TestClient.printUsage(options);
                System.exit(-1);
            }
            String s = line.getOptionValue("speaker");
            int speaker = 0;
            if(s.matches("\\d+")){
                speaker = Integer.parseInt(s);
                this.setSpeaker(speaker);
            }else{
                logger.error("speaker parsing error. " + s);
                TestClient.printUsage(options);
                System.exit(-1);
            }
            this.speaker = speaker;
            String t = line.getOptionValue("text");
            this.text = t;

            String out = line.getOptionValue("out");
            this.outfile = out;
        }catch(Exception e){
            logger.error("{}", e.getMessage());
        }

        logger.info("ip:{}, port:{}",ip, port);

        downloadWav();
    }

    //blocking
    private void downloadWav(){
        ManagedChannel channel = ManagedChannelBuilder.forAddress(ip, port).usePlaintext()
//                .keepAliveWithoutCalls(true).keepAliveTime(30, TimeUnit.SECONDS)
                .build();
        try {
            NgTtsServiceGrpc.NgTtsServiceBlockingStub stub = NgTtsServiceGrpc.newBlockingStub(channel);

            TtsRequest.Builder req = TtsRequest.newBuilder();
            req.setLang(LangOuterClass.Lang.ko_KR);
            req.setSampleRate(16000);
            req.setSpeaker(speaker);
            req.setText(text);

            long st = 0;
            if (download) {
                try {
                    LangOuterClass.Lang lang = convertLang(this.lang);
                    req.setLang(lang);

                    st = System.currentTimeMillis();
                    logger.info("download wav file..start=========");

                    File out = new File(outfile);
                    if(out.exists()){
                        FileUtils.deleteQuietly(out);
                    }

                    Iterator<TtsMediaResponse> iter = stub.withDeadlineAfter(300, TimeUnit.SECONDS)
                            .speakWav(req.build());
                    while(iter.hasNext()) {
                        TtsMediaResponse resp = iter.next();
                        ByteString data = resp.getMediaData();
                        saveFile(data);
                    }

//                    TtsMediaResponse resp = stub
//                            //.withDeadlineAfter(300, TimeUnit.SECONDS)
//                            .speakWav(req.build());
//                    ByteString data = resp.getMediaData();
//                    saveFile(data);
                    logger.info("download wav file..finished========= elapsed: {}", (System.currentTimeMillis() - st) + "");
                } catch (StatusRuntimeException e) {
                    logger.error("{},{}", e.toString(),(System.currentTimeMillis() - st) );
                } catch (Exception e) {
                    logger.error("{}", e.getMessage());
                }
            }
        }catch(Exception e){
            logger.error("downloadWav/{}",e.getMessage());
        }

        try{
            channel.shutdown().awaitTermination(300, TimeUnit.SECONDS);
        }catch (InterruptedException e){

        }catch (Exception e ){

        }
    }

    private void saveFile(ByteString data){
        if(data == null){
            return;
        }
        ByteBuffer buff = data.asReadOnlyByteBuffer();
        try (
                FileOutputStream fos = new FileOutputStream(outfile, true);
                FileChannel fc = fos.getChannel();

        ) {
            fc.write(buff);
//            logger.debug("completed. {} =========", outfile);
        } catch (Exception e) {
//            e.printStackTrace();
            logger.debug("error:{}", e.getMessage());
        }

    }

    public static void printUsage(Options options){
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "help", options );
    }

    public static class CliLogger {

        public void warn(Object... args){
            log("warn", args);
        }
        public void debug(Object... args){
            log("debug", args);
        }
        public void error(Object... args){
            log("error", args);
        }
        public void info(Object... args){
            log("info", args);
        }
        private void log(String level, Object... args){
            if(args == null || args.length == 0){
                return;
            }
            String main = args[0]+"";
            String out = main;
            for(int i=1;i<args.length;i++){
                String ar = "";
                if(args[i] instanceof Integer || args[i] instanceof Long) {
                    ar = "" + args[i];
                }else{
                    ar = (String)args[i];
                }
                out = out.replaceFirst("\\{\\}", ar);
            }
            System.out.println(level+"/"+out);
        }
    }

    private LangOuterClass.Lang convertLang(String lang) throws Exception{
        if("ko_KR".equals(lang)){
            return LangOuterClass.Lang.ko_KR;
        }else if("en_US".equals(lang)){
            return LangOuterClass.Lang.en_US;
        }else{
            throw new Exception("Invalid Language Code");
        }
    }
}
